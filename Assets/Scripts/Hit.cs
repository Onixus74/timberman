﻿using UnityEngine;
using System.Collections;

public class Hit : MonoBehaviour {

	static public string hitPosition ; 
	public bool currentTarget;
	private GameObject player;
	public bool canHit;
	// Use this for initialization
	void Start () {
		currentTarget = false;
		player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		isCurrent();

		if(currentTarget  && Input.GetKeyDown(KeyCode.LeftArrow)){
			HitLeft();
		}else 
		if(!currentTarget  && Input.GetKeyDown(KeyCode.LeftArrow)){
			transform.Translate(new Vector2 (0 , -1.5f ));
		}

		if(currentTarget  && Input.GetKeyDown(KeyCode.RightArrow)){
			HitRight();
		}else 
		if(!currentTarget  && Input.GetKeyDown(KeyCode.RightArrow)){
			transform.Translate(new Vector2 (0 , -1.5f ));
		}

		if(transform.position.x == 3){
			transform.position = new Vector2 (0,18.0f);
		}

		if(transform.position.x == -3){
			transform.position = new Vector2 (0,18.0f);
		}


		
	}

	void isCurrent(){
		if(transform.position.y == player.gameObject.transform.position.y){
			currentTarget = true;
		}else{
			currentTarget = false;
		}
	}

	void HitLeft (){

		Debug.Log("Hit Left");
		//iTween.MoveTo(gameObject,new Vector2(3,-1),0.5f);
		iTween.MoveTo(gameObject, iTween.Hash("x", 3 ,"y" , -1.7 , "easetype", "linear" , "time" , 0.2f));
		iTween.RotateTo(gameObject,new Vector3(0,0,0),0.5f);
		currentTarget = false;
	}

	void HitRight (){
		Debug.Log("Hit Right");
		//iTween.MoveTo(gameObject,new Vector2(-3,-1),0.5f);
		iTween.MoveTo(gameObject, iTween.Hash("x", -3 ,"y" , -1.7 , "easetype", "linear" , "time" , 0.2f));
		iTween.RotateTo(gameObject,new Vector3(0,0,0),0.5f);
		currentTarget = false;
	}


}
