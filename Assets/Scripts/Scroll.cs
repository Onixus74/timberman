﻿using UnityEngine;
using System.Collections;

public class Scroll : MonoBehaviour {
	static public bool isCurrent;
	 
	// Use this for initialization
	void Start () {
		isCurrent = false;
	}
	
	// Update is called once per frame
	void Update () {

		if(!PlayerBehaviour.isDead)
			Scrolling();
	
	}
	void Scrolling(){
		if(Input.GetKeyDown(KeyCode.LeftArrow)){
			transform.Translate(new Vector2 (0 , -1.5f ));
		}
		
		if(Input.GetKeyDown(KeyCode.RightArrow)){
			transform.Translate(new Vector2 (0 , -1.5f ));
		}
		
		if(transform.position.y == -4.5){
			transform.position = new Vector2 (0,18.0f);
		}
		
		if(transform.position.y == GameObject.FindGameObjectWithTag("Player").transform.position.y){
			isCurrent= true;
			gameObject.tag="Current";
		}
		
		else{
			isCurrent=false;
			gameObject.tag=null;
		}
	}

}
