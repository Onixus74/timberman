using UnityEngine;
using System.Collections;

public class PlayerBehaviour : MonoBehaviour {
	static public bool isDead ; 
	static public string playerPosition;
	public string currentDirection ; 
	public int score ;
	// Use this for initialization
	void Start () {
		playerPosition = "Standing";
		transform.position = new Vector2 (-2,-3); 
		isDead = false;
		score = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(!isDead)
		KeyBoardControl();
		if(isDead){
			Debug.Log("Player is Dead");
		}


		 if(GameObject.FindGameObjectWithTag("Current").transform.FindChild("obstacle").localPosition.x == -1){
			currentDirection="left";
		}

		if(GameObject.FindGameObjectWithTag("Current").transform.FindChild("obstacle").localPosition.x == 1){
			currentDirection="right";
		}



		if(playerPosition == currentDirection ) {

			isDead=true;
		}
	}

	void KeyBoardControl (){
		if(Input.GetKeyDown(KeyCode.RightArrow)){
			playerPosition="right";
			ChangePosition();
			score ++ ;
		}
		if(Input.GetKeyDown(KeyCode.LeftArrow)){
			playerPosition="left"; 
			ChangePosition();
			score++;
		}

	}


	void ChangePosition(){
		if(playerPosition=="right"){
			transform.position = new Vector2 (2,-3);
		}
		if(playerPosition=="left"){
			transform.position = new Vector2 (-2,-3);
		}
	}

	void OnGUI(){
		if(isDead){
			GUI.Box(new Rect ( Screen.width/2-100,  Screen.height/2-50, 200, 130),"Score "  + score);
			if(GUI.Button(new Rect(Screen.width/2-50, Screen.height/2-15, 100, 30), "Replay") )
			{
				Application.LoadLevel(Application.loadedLevel);
				
			}
			if(GUI.Button(new Rect(Screen.width/2-50, (Screen.height/2-15)+30, 100, 30), "Share") )
			{
				//Load level selector
				
			}
			if(GUI.Button(new Rect(Screen.width/2-50, (Screen.height/2-15)+60, 100, 30), "Change Character") )
			{
				//Load level selector
				
			}
		}
	}



}