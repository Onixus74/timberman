﻿using UnityEngine;
using System.Collections;

public class Cut : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if(!PlayerBehaviour.isDead)
			InputController();
		
	}

	void HitLeft (){
		
		transform.position = new Vector2(0,-3);

		//iTween.MoveTo(gameObject,new Vector2(3,-1),0.5f);
		iTween.MoveTo(gameObject, iTween.Hash("x", 3 ,"y" , -2.0f , "easetype", "linear" , "time" , 0.1f));
		iTween.RotateTo(gameObject,new Vector3(0,0,180),0.5f);
	}
	
	void HitRight (){
		transform.position = new Vector2(0,-3);
		//iTween.MoveTo(gameObject,new Vector2(-3,-1),0.5f);
		iTween.MoveTo(gameObject, iTween.Hash("x", -3 ,"y" , -2.0f , "easetype", "linear" , "time" , 0.1f));
		iTween.RotateTo(gameObject,new Vector3(0,0,180),0.5f);


		
	}

	void InputController(){
		if(Input.GetKeyDown(KeyCode.LeftArrow)){
			HitLeft();
		}
		
		if(Input.GetKeyDown(KeyCode.RightArrow)){
			HitRight();
		}
		
		
		
		if(transform.position.x == -3)
			transform.position = new Vector2(0,-12);
		
		if(transform.position.x == 3)
			transform.position = new Vector2(0,-12);
	}
}
