﻿using UnityEngine;
using System.Collections;

public class obstacle : MonoBehaviour {
	int randomValue ;
	public string direction;
	// Use this for initialization
	void Start () {
		direction = "none";

		randomValue = Random.Range(1,3);
		if(randomValue == 1){
			transform.localPosition = new Vector3 (-1,0,0.1f);
			direction = "left";
		}
		
		if(randomValue == 2){
			transform.localPosition = new Vector3 (1,0,0.1f);
			direction = "right";
		}
	}
	
	// Update is called once per frame
	void Update () {
		randomValue = Random.Range(1,3);
		if(transform.parent.position.y == 18){

			if(randomValue == 1){
				transform.localPosition = new Vector3 (-1,0,0.1f);
				direction = "left";
			}

			if(randomValue == 2){
				transform.localPosition = new Vector3 (1,0,0.1f);
				direction = "right";
			}

		}



	}


	void OnTriggerEnter2D(Collider2D collision){
		if(collision.gameObject.tag=="Player"){
			PlayerBehaviour.isDead = true;
		}
	}
}
